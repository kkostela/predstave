from django.shortcuts import render
from .models import  *
# Create your views here.

def predstave(request):
    predstava = Predstava.objects.all()
    context = {'predstava':predstava}
    return render(request, 'predstave/predstave.html', context)

def kosarica(request):
    context = {}
    return render(request, 'predstave/kosarica.html', context)

def dostava(request):
    context = {}
    return render(request, 'predstave/dostava.html', context)