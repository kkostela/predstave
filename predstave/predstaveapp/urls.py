from django.urls import path
from . import views

urlpatterns = [
    path('', views.predstave, name="predstave"),
    path('kosarica/', views.kosarica, name="kosarica"),
    path('dostava/', views.dostava, name="dostava")
]