from django.contrib import admin
from .models import *

# Register your models here.

admin.site.register(Korisnik)
admin.site.register(Predstava)
admin.site.register(Evidencija)
admin.site.register(StavkeEvidencije)
admin.site.register(Adresa)

