from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class Korisnik(models.Model):
    user = models.OneToOneField(User, null=True, blank=True, on_delete=models.CASCADE)
    ime = models.CharField(max_length=300, null=True)
    email = models.CharField(max_length=300)

    def __str__(self):
        return self.ime

class Predstava(models.Model):
    naziv = models.CharField(max_length=200)
    cijena = models.FloatField()
    slike = models.ImageField(null=True, blank=True)
    datumPredstave = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.naziv

class Evidencija(models.Model):
    korisnik = models.ForeignKey(Korisnik, on_delete=models.SET_NULL, null=True, blank=True)
    datum = models.DateTimeField(auto_now_add=True)
    transakcijski_id = models.CharField(max_length=100, null=True)
    zavrsen = models.BooleanField(default=False)

    def __str__(self):
        return str(self.id)

class StavkeEvidencije(models.Model):
    predstava = models.ForeignKey(Predstava, on_delete=models.SET_NULL, null=True)
    evidencija = models.ForeignKey(Evidencija, on_delete=models.SET_NULL, null=True)
    kolicina = models.IntegerField(default=0, null=True, blank=True)
    datum_dodavanja = models.DateTimeField(auto_now_add=True)

class Adresa(models.Model):
    korisnik = models.ForeignKey(Korisnik, on_delete=models.SET_NULL, null=True)
    evidencija = models.ForeignKey(Evidencija, on_delete=models.SET_NULL, null=True)
    adresa = models.CharField(max_length=200, null=False)
    grad = models.CharField(max_length=200, null=False)
    drzava = models.CharField(max_length=200, null=False)
    datum_slanja = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.adresa





